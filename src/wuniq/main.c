/*
** main.c for  in /home/wapiflapi/Projects/punk
**
** Made by Wannes Rombouts
** Login   <rombou_w@epitech.net>
**
** Started on  Sat Jul  7 01:39:08 2012 Wannes Rombouts
** Last update Sun Jul  8 00:35:25 2012 Wannes Rombouts
*/

#define		_LARGEFILE64_SOURCE

#include	<unistd.h>
#include	<stdio.h>
#include	<math.h>
#include	<err.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<string.h>
#include	"punk.h"

static int	filter_file(t_punk *punk, char const *file, uint64_t n, int bufsz)
{
  int		fd;
  ssize_t	r;

  char		*rbuffer;
  char		*wbuffer;

  char		*rrp, *wwp;
  uint64_t	i, chunk_nb;
  off64_t	next_chunk, read_off, write_off;

  struct stat	sbuf;
  off64_t	size;
  int64_t	nblines = -1;

  if (!(rbuffer = malloc(bufsz)) || !(wbuffer = malloc(bufsz)))
    err(EXIT_FAILURE, "malloc buffers");

  if ((fd = open(file, O_RDWR)) < 0)
    err(EXIT_FAILURE, "open(%s)", file);

  if (fstat(fd, &sbuf))
    err(EXIT_FAILURE, "stat(%s)", file);

  size = sbuf.st_size;

  read_off = 0;
  write_off = 0;

  i = 0;
  chunk_nb = 0;

  /* loop for every chunk. */
  while (1)
    {
      int	b = 0;

      next_chunk = 0;
      rrp = rbuffer;

      /* loop over file */
      while ((r = read(fd, rrp, bufsz - (rrp - rbuffer))) > 0)
	{
	  char	*rp;
	  char	*ep;

	  ++b;
	  printf("\rPass %3lu / %-3.d ( buf %-3d %3.d%% )", (unsigned long) chunk_nb,
		 nblines < 0 ? -1 : (int) (nblines / n),
		 b, (int) (bufsz * b * 100.0 / size));

	  fflush(stdout);

	  ep = rbuffer + (rrp - rbuffer) + r;
	  rp = rbuffer;
	  wwp = wbuffer;

	  /* loop over lines. */
	  while (rp < ep)
	    {
	      unsigned int l;

	      for (l = 0; rp + l < ep && rp[l] != '\n'; ++l)
		{;}

	      if (rp + l >= ep || rp[l] != '\n')
		break; /* not a full line anymore. */

	      rp[l] = 0;

	      if (i < (chunk_nb + 1) * n) /* main chunk */
		{
		  if (!punk_lookup(punk, rp))
		    {
		      /* if not in filter we should write to file and to filter. */
		      punk_insert(punk, rp);
		      rp[l] = '\n';
		      wwp = memcpy(wwp, rp, l + 1) + l + 1;
		    }
		}

	      else
		{
		  if (i == (chunk_nb + 1) * n) /* next chunk is here */
		    next_chunk = write_off + (wwp - wbuffer);

		  if (!punk_lookup(punk, rp) || 1)
		    {
		      /* if not in filter we should write to file */
		      rp[l] = '\n';
		      wwp = memcpy(wwp, rp, l + 1) + l + 1;
		    }
		}

	      i += 1;
	      rp += l + 1; /* skip \n */
	    }

	  if (rp == rbuffer)
	    errx(EXIT_FAILURE, "Nothing to do in %u byte rbuffer -- abort\n"
		 "File may be left in a corrupt state.", bufsz);

	  /* dumping write buffer to file. */
	  read_off = lseek64(fd, 0, SEEK_CUR);
	  lseek64(fd, write_off, SEEK_SET);
	  write_off += write(fd, wbuffer, wwp - wbuffer);
	  lseek64(fd, read_off, SEEK_SET);

	  /* adjusting read buffer for next round */
	  rrp = rbuffer + (bufsz - (rp - rbuffer));
	  memmove(rbuffer, rp, rrp - rbuffer);
	}

      if (ftruncate(fd, write_off))
	errx(EXIT_FAILURE, "truncate() failed: %m\n"
	     "File may be left in a corrupt state.");

      nblines = i;
      chunk_nb += 1;
      if (!next_chunk)
	break;

      /* going back to next chunk. */
      lseek64(fd, next_chunk, SEEK_SET);
      write_off = next_chunk;
      i = chunk_nb * n;

      /* star bloom filter again. */
      punk_clean(punk);
    }

  printf("\rDone. %llu passes, %lld entries\n",
	 (unsigned long long) chunk_nb, (long long) nblines);
  close(fd);
  return 0;
}

int		main(int argc, char **argv)
{
  uint64_t	m, n, k;
  double	p;
  t_punk	*punk;
  int		bufsz;

  if (argc != 5)
    errx(EXIT_FAILURE,
	 "Usage: %s FILE NB_Mb RD_Mb ERRFLOAT\n"
	 "\tFILE:     The file to be filtered.\n"
	 "\tNB_Mb:    How much memory should be used by the algorithm (Mb).\n"
	 "\tRD_Mb:    How much memory should be used by the buffers (Mb).\n"
	 "\tERRFLOAT: Max false positive rate. (eg 0.00001)",
	 argv[0]);

  m = atoll(argv[2]) * 8 * 1024 * 1024;
  p = atof(argv[4]);
  n = m / (-log1p(p - 1) / (M_LN2 * M_LN2));
  k = m * M_LN2 / n;
  bufsz = atol(argv[3]) * 1024 * 1024 / 2;

  if (!(punk = punk_init(m, k)))
    errx(EXIT_FAILURE, "punk_init failed. Probably not enough memory.\n");

  printf("wuniq v0.1 - @wapiflapi - wapiflapi@yahoo.fr\n"
	 "Mem %luMb, pErr %f, n/chunk %llu, kHash %llu, bufs 2*%dMb\n",
	 (unsigned long) m / 8 / 1024 / 1024, p, (unsigned long long) n,
	 (unsigned long long) k, bufsz / 1024 / 1024);

  filter_file(punk, argv[1], n, bufsz);

  return (0);
}
