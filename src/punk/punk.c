/*
** punk.c for  in /home/wapiflapi/Projects/punk
**
** Made by Wannes Rombouts
** Login   <rombou_w@epitech.net>
**
** Started on  Sat Jul  7 01:38:36 2012 Wannes Rombouts
** Last update Sat Jul  7 22:36:16 2012 Wannes Rombouts
*/

#include		"punk.h"
#include		<string.h>

t_punk			*punk_init(uint64_t m, unsigned int k)
{
  t_punk		*punk;

  if (!(punk = calloc(1, sizeof *punk + (m >> 3))))
    return NULL;

  punk->m = m;
  punk->k = k;

  return punk;
}

void			punk_insert(t_punk *punk, char const *data)
{
  unsigned int		i;
  uint64_t		h;
  uint64_t		f, a;

  f = punkfnv(data);
  a = punkfnva(data);
  i = punk->k;
  while (i--)
    {
      h = (f + i * a) % punk->m;
      punk->data[h >> 3] |= (1 << (h & 7));
    }
}

int			punk_lookup(t_punk *punk, char const *data)
{
  uint64_t		h;
  unsigned int		i;
  uint64_t		f, a;

  f = punkfnv(data);
  a = punkfnva(data);
  i = punk->k;
  while (i--)
    {
      h = (f + i * a) % punk->m;
      if (!(punk->data[h >> 3] & (1 << (h & 7))))
	return (0);
    }
  return (1);
}

void			punk_clean(t_punk *punk)
{
  memset(punk->data, 0, punk->m / 8);
}
