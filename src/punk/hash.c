/*
** hash.c for  in /home/wapiflapi/Projects/punk
**
** Made by Wannes Rombouts
** Login   <rombou_w@epitech.net>
**
** Started on  Sat Jul  7 02:01:18 2012 Wannes Rombouts
** Last update Sun Jul  8 00:23:54 2012 Wannes Rombouts
*/

#include		"punk.h"
#include		<stdio.h>

# define		FNVBASE (uint64_t) 14695981039346656037ull
# define		FNVPRIM (uint64_t) 1099511628211ull

uint64_t		punkfnv(char const *data)
{
  uint64_t		h = FNVBASE;

  for (; *data; ++data)
    {
      h = h * FNVPRIM;
      h = h ^ *data;
    }

  return h;
}

uint64_t		punkfnva(char const *data)
{
  uint64_t		h = FNVBASE;

  for (; *data; ++data)
    {
      h = h ^ *data;
      h = h * FNVPRIM;
    }

  return h;
}

/*
** This is not called.
** Instead we do this inline by facotrising the code in insert
** and lookup routines.
*/
uint64_t		punk_hash(char const *data, uint64_t k, uint64_t m)
{
  uint64_t		h;

  h = punkfnv(data);
  while (k--)
    h = h + punkfnva(data);

  return h % m;
}
