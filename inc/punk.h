/*
** punk.h for  in /home/wapiflapi/Projects/punk
**
** Made by Wannes Rombouts
** Login   <rombou_w@epitech.net>
**
** Started on  Sat Jul  7 01:39:39 2012 Wannes Rombouts
** Last update Sat Jul  7 22:49:10 2012 Wannes Rombouts
*/

#ifndef			__PUNK_H__
# define		__PUNK_H__

#include		<stdint.h>
# include		<stdlib.h>

typedef struct		s_punk
{
  uint64_t		m;
  unsigned int		k;
  char			data[];
}			t_punk;

t_punk			*punk_init(uint64_t m, unsigned int k);
uint64_t		punkfnv(char const *data);
uint64_t		punkfnva(char const *data);
uint64_t		punk_hash(char const *data, uint64_t k, uint64_t m);
void			punk_insert(t_punk *punk, char const *data);
int			punk_lookup(t_punk *punk, char const *data);
void			punk_clean(t_punk *punk);

#endif
