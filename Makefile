##
## Makefile for  in /home/wapiflapi
##
## Made by Wannes Rombouts
## Login   <rombou_w@epitech.net>
##
## Started on  Thu Mar  8 16:15:20 2012 Wannes Rombouts
## Last update Sat Jul  7 23:40:46 2012 Wannes Rombouts
##

HOSTTYPE?=	$(shell uname -m)-$(shell uname -s)
PRGNAME=	bin/$@
LIBNAME=	lib/lib$@.a

CC=		cc
RM=		rm -f
LN=		ln -fs
ARRC=		ar rc
RANLIB=		ranlib

CC_FLAGS:=	-fPIC -Wall -Wextra -Werror -Iinc -O3 -DNDEBUG
LD_FLAGS:=	-Llib -l

PUNK_SRCS=	$(wildcard src/punk/*.c)
WUNIQ_SRCS=	$(wildcard src/wuniq/*.c)

PUNK_OBJS=	$(patsubst src/%.c, obj/%.o, $(PUNK_SRCS))
WUNIQ_OBJS=	$(patsubst src/%.c, obj/%.o, $(WUNIQ_SRCS))

TODO=		punk wuniq

all:		$(TODO)

obj/%.o:	src/%.c
		$(CC) $(CC_FLAGS) -c -o $@ $<

punk:		$(PUNK_OBJS)
		$(ARRC) $(LIBNAME) $(PUNK_OBJS)
		$(RANLIB) $(LIBNAME)

wuniq:		$(WUNIQ_OBJS) punk
		$(CC) -o $(PRGNAME) $(WUNIQ_OBJS) $(CC_FLAGS) $(LD_FLAGS)punk -lm
		$(LN) $(PRGNAME) $(patsubst bin/%,%,$(PRGNAME))

clean:
		$(RM) obj/*/*

fclean:		clean
		$(RM) $(TODO) bin/* lib/*

re:		fclean all
