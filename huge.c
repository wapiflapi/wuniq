/*
** huge.c for  in /home/wapiflapi/Projects/punk
**
** Made by Wannes Rombouts
** Login   <rombou_w@epitech.net>
**
** Started on  Sat Jul  7 06:07:27 2012 Wannes Rombouts
** Last update Sat Jul  7 23:57:39 2012 Wannes Rombouts
*/

#include	<stdio.h>

/*
** A simple little helper to generate big files.
*/

int		main(int argc, char **argv)
{
  FILE		*huge;

  huge = fopen(argv[1] ? argv[1] : "huge_file", "w");
  while (1)
    fprintf(huge, "%d%d\n", random() % 0xFFFFFF, random() % 0xFFFFFF);
  fclose(huge);
  return 0;
}
